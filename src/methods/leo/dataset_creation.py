import csv
import requests
import os

# Chemin du fichier CSV
csv_file_path = '..\\published_images.csv'

# Dossier de destination pour enregistrer les images
destination_folder = 'dataset/'

# Nombre de lignes à télécharger
nombre_lignes_a_telecharger = 500

# Vérifiez si le dossier de destination existe, sinon créez-le
if not os.path.exists(destination_folder):
    os.makedirs(destination_folder)

# Ouvrir le fichier CSV
with open(csv_file_path, newline='', encoding='utf-8') as csvfile:
    # Lire le fichier CSV
    csv_reader = csv.reader(csvfile)
    
    # Ignorer l'en-tête si nécessaire
    next(csv_reader, None)
    
    # Parcourir les 1000 premières lignes du CSV
    for i, row in enumerate(csv_reader):
        if i >= nombre_lignes_a_telecharger:
            break  # Sortir de la boucle une fois que le nombre de lignes souhaité est atteint

        print(f"Téléchargement de l'image {i + 1}...")

        # Récupérer l'URL de l'image depuis la deuxième colonne (index 1)
        image_url = row[2]
        print("img url : ",image_url)
        # Récupérer le nom de fichier depuis la première colonne (index 0)
        image_filename = row[0] + '.jpg'
        
        # Chemin complet du fichier destination
        destination_path = os.path.join(destination_folder, image_filename)

        try:
            # Télécharger l'image et l'enregistrer localement
            response = requests.get(image_url)
            response.raise_for_status()  # Vérifier si la requête a réussi

            with open(destination_path, 'wb') as img_file:
                img_file.write(response.content)
                #print(f"Chemin complet du fichier destination : {destination_path}")

            print(f"Image {i + 1} téléchargée avec succès.")
        except Exception as e:
            print(f"Erreur lors du téléchargement de l'image {i + 1}: {e}")